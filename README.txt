********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Buddylist2
Author: Dominik Kiss <dominik.kiss[at]gmx.at> nodestroy at drupal.org
Thanks to: dkruglyak at drupal.org
Drupal Version: 5
********************************************************************
DESCRIPTION:

Buddylist2 is a collection of buddy related modules. These are:
- Buddy API
- Buddylist UI
- Buddy API Shortest Route
- Buddy API Invite

For more information about this modules please look at the specific
README files.


